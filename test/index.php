<?php

require_once __DIR__.'/../vendor/autoload.php';

use Vector5\DB\TreeWalker\PDOTreeWalker;

$db = 'studentpulse2';
$pdo = new PDO("pgsql:host=localhost;port=5432;dbname=$db;user=postgres;password=qwerty");

$tw = new PDOTreeWalker($pdo);

$r =  $tw->load('studentpulse2', ['schema' => 'public']);

print_r($r->toArray());