<?php

require_once __DIR__.'/../vendor/autoload.php';

use Vector5\DB\TreeWalker\Postgres\PDOTreeWalker;

$db = 'studentpulse2';
$pdo = new PDO("pgsql:host=localhost;port=5432;dbname=$db;user=postgres;password=qwerty");

$tw = new PDOTreeWalker($pdo);

$s1 = $tw->load($db);
$s2 = $tw->load($db);

$s2->getTableCollection()->forget('survey_responses');
$s2->addTable('added_table');
$s2->getTable('engagements')->getColumnCollection()->forget('created_at');
$s2->getTable('engagements')->addColumn('xxxx', 'character varying');
$s2->getTable('active_engagements')->addColumn('created_at', 'integer');

$r = array_map('strval', $tw->revisions($s1, $s2));

echo json_encode($r, JSON_PRETTY_PRINT), "\n";