import $ from 'jquery'
import defaults from 'lodash/defaults'

import TreeWalkerParameterBuilder from './TreeWalkerParameterBuilder'
import { bindMethods, callbacks } from './utils'
import { getClassName, getSelector } from './parameter-utils'
import { renderTypeOptions, renderParameterBuilderName, renderParameterBuilderLabel } from './defaults'

const mergeDefaultOptions = (options) => defaults(options, {
  renderTypeOptions,
  renderParameterName: renderParameterBuilderName,
  renderParameterLabel: renderParameterBuilderLabel
})

class TreeWalkerParameterListBuilder {
  constructor(options) {
    options = mergeDefaultOptions(options)

    this.$element = $(options.element)
    this.inputName = options.inputName
    this.parameterTypes = options.parameterTypes
    this.parameters = []
    this.nextIndex = 0

    // Create callbacks for each known events and merge its methods to our instance
    Object.assign(this, callbacks(['parameterAdded', 'parameterChanged']))

    bindMethods(this, options, ['renderTypeOptions', 'renderParameterName', 'renderParameterLabel'])
  }

  add(data = null) {
    let parameter = new TreeWalkerParameterBuilder(this, {
      parameterTypes: this.parameterTypes,
      inputName: this.inputName,
      index: this.nextIndex,
      renderParameterName: this.renderParameterName,
      renderTypeOptions: this.renderTypeOptions,
      renderParameterLabel: this.renderParameterLabel
    })

    this.parameters.push(parameter)
    // Use our callbacks to fire events
    parameter.setCallbacks(this)
    
    this.$element.find(getSelector('list')).append(parameter.render())
    this.nextIndex++

    // Load if initial data was specified
    if (data) {
      parameter.load(data) 
    } 

    this.fire('parameterAdded', parameter)
    
    return parameter
  }

  remove(index) {
    if (typeof this.parameters[index] !== 'undefined') {
      const parameter = this.parameters[index]
      this.parameters.splice(index, 1)

      return parameter
    }

    return false
  }

  validate() {
    let valid = false

    for (let i = 0; i < this.parameters.length; i++) {
      let parameter = this.parameters[i]
      valid = valid && parameter.validate()
    }

    return valid
  }

  count() {
    return this.parameters.length
  }

  load(parameters) {
    parameters.forEach(parameter => this.add(parameter))
  }

  render() {
    this.$element.html(`
    <div class="${getClassName('wrapper')}">
      <ul class="${getClassName('list')}"></ul>
      <button class="btn btn-primary ${getClassName('add')}" type="button">Add Parameter</button>
    </div>
    `)

    this.$element.on('click', getSelector('add'), (evt) => {
      evt.preventDefault()
      this.add()
    })
  }

  getElement() {
    return this.$element
  }
}

export default TreeWalkerParameterListBuilder