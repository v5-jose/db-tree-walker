import { getClassName } from './parameter-utils'

export function renderTypeOptions(parameter, types) {
  let $select = $(`<select class="${getClassName('type-select')}"></select>`)

  $select.attr('name', parameter.getInputName('type'))
  $select.append('<option value="">--Select Type--</option>')

  $select.append(types.map(type => 
    $('<option></option>').val(type.name).text(type.label)
  ))

  return $select
}

export function renderParameterBuilderName(parameter, inputName) {
  return $('<input />')
    .attr({
      type: 'text',
      name: inputName,
      placeholder: "Enter parameter name"
    })
    .addClass(getClassName('parameter-name-input'))
}

export function renderParameterBuilderLabel(parameter, inputName) {
  return $('<input />')
    .attr({
      type: 'text',
      name: inputName,
      placeholder: "Enter parameter label"
    })
    .addClass(getClassName('parameter-label-input'))
}