import noop from 'lodash/noop'

import { renderInput as defaultRenderInput, getTypeSelector, renderLabeledElement } from './parameter-utils'

export const createType = (name, label, props) => {
  return {
    name,
    label,
    validateBuilder: () => false,
    loadBuilder: noop,
    load: noop,
    setValue: (parameter, value) => value || parameter.getDefaultValue(),
    getValue: (parameter, value) => value || parameter.getDefaultValue(),
    ...props
  }
}

export const createText = (renderInput = defaultRenderInput) => {
  const TYPE_TEXT = 'string'
  const createTextInput = () => renderInput(TYPE_TEXT).attr('type', 'text')

  return createType(TYPE_TEXT, 'Text', {
    loadBuilder: (parameter, config) => {
      parameter.getElement()
        .find(getTypeSelector(TYPE_TEXT))
        .val(config.default)
    },
    renderBuilder: (parameter) => {
      return createTextInput()
        .attr('name', parameter.getInputName('default'))
        .attr('placeholder', 'Enter default value')
    },
    setValue: (parameter, value) => {
      parameter.getElement().find(
        ':input[name="' + parameter.getInputName('value') + '"]'
      ).val(value)

      return value
    },
    render: (parameter) => renderLabeledElement(parameter, [
      parameter.renderParameterNameHiddenInput(),
      createTextInput()
        .attr('name', parameter.getInputName('value'))
        .val(parameter.getValue())
        .addClass('form-control')
    ])
  })
}

export const createNumber = (renderInput = defaultRenderInput) => {
  const TYPE_NUMBER = 'number'
  const createNumberInput = () => renderInput(TYPE_NUMBER).attr('type', 'number')
  
  return createType(TYPE_NUMBER, 'Number', {
    loadBuilder: (parameter, config) => {
      parameter.getElement()
        .find(getTypeSelector(TYPE_NUMBER))
        .val(config.default)
    },
    renderBuilder: (parameter) => {
      return $('<div />').append([
        createNumberInput()
          .attr('name', parameter.getInputName('default'))
          .attr('placeholder', 'Enter default value')
      ])
    },
    setValue: (parameter, value) => {
      parameter.getElement().find(
        ':input[name="' + parameter.getInputName('value') + '"]'
      ).val(value)

      return value
    },
    render: (parameter) => renderLabeledElement(parameter, [
      parameter.renderParameterNameHiddenInput(),
      createNumberInput()
        .attr('name', parameter.getInputName('value'))
        .val(parameter.getValue())
        .addClass('form-control')
    ])
  })
}