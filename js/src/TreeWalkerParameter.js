import $ from 'jquery'
import defaults from 'lodash/defaults'

import { getClassName } from './parameter-utils'

const mergeDefaultOptions = (options) => defaults(options, {
  value: null
})

class TreeWalkerParameter {
  constructor(options) {
    options = mergeDefaultOptions(options)

    this.type = options.type
    this.inputName = options.inputName
    this.config = options.config  
    this.value = options.value
    this.index = options.index
 
  }

  getInputName(key = '') {
    return `${this.inputName}[${this.index}]` + (key ? `[${key}]` : '')
  }

  getLabel() {
    return this.config.label
  }

  getDefaultValue() {
    return this.config.default
  }

  setValue(value) {
    this.value = this.type.setValue(this, value)

    return this
  }

  getValue() {
    return this.type.getValue(this, this.value)
  }

  getName() {
    return this.config.name
  }

  renderParameterNameHiddenInput() {
    return $('<input />')
      .attr({
        type: 'hidden',
        name: this.getInputName('name')
      })
      .val(this.config.name)
  }

  getElement() {
    if (! this.$element) {
      this.$element = $('<li></li>')
        .addClass(getClassName('list-item'))
        .addClass(getClassName('list-item-' + this.type.name))
        .append(this.type.render(this))
    }

    return this.$element
  }

  render() {
    return this.getElement()
  }
}

export default TreeWalkerParameter