import $ from 'jquery'
import defaults from 'lodash/defaults'
import find from 'lodash/find'

import TreeWalkerParameter from './TreeWalkerParameter'
import { getTypeFromList, getClassName } from './parameter-utils'

const mergeDefaultOptions = (options) => defaults(options, {
  inputName: 'treewalker_parameters',
  values: {}
})

class TreeWalkerParameterList {
  constructor(options) {
    options = mergeDefaultOptions(options)
 
    this.$element = $(options.element)
    this.$title = $(options.title)
    this.parameterTypes = options.parameterTypes
    this.inputName = options.inputName
    this.parameters = []
    this.$wrapper = $('<ul></ul>').addClass(getClassName('list'))
 
    if (options.parameters instanceof Array) {
      this.loadParameters(options.parameters, options.values)
    }
  }

  /**
   * 
   * @param {Array} parameters
   * @param {Array} values
   * @return {void} 
   */
  loadParameters(parameters, values) {
    this.parameters = parameters
      .map((config, index) => new TreeWalkerParameter({
        type: getTypeFromList(this.parameterTypes, config.type),
        inputName: this.inputName,
        value: typeof values[config.name] !== 'undefined' ? values[config.name] : null,
        config,
        index
      }))

    return this
  }

  getParameterByName(name) {
    return find(this.parameters, parameter => parameter.getName() === name)
  }

  setValues(values) {
    if (this.parameters.length > 0) {
      values.forEach(value => {
        const parameter = this.getParameterByName(value.name)
        if (parameter) {
          parameter.setValue(value.value) 
        }
      }) 
    }

    return this
  }

  render() {  
    this.$element.append([
      this.$title,
      this.$wrapper.append(
        this.parameters.map(parameter => parameter.render())
      )
    ])
  }
}

export default TreeWalkerParameterList