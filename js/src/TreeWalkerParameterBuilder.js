import $ from 'jquery'
import defaults from 'lodash/defaults'

import { getTypeFromList, getSelector, getClassName } from './parameter-utils'

const mergeDefaultOptions = (options) => defaults(options, {
  config: {}
})

class TreeWalkerParameterBuilder {
  constructor(manager, options) {
    this.manager = manager
    options = mergeDefaultOptions(options)

    this.parameterTypes = options.parameterTypes
    this.inputName = options.inputName
    this.index = options.index
    this.config = options.config
    this.renderTypeOptions = options.renderTypeOptions
    this.renderParameterName = options.renderParameterName
    this.renderParameterLabel = options.renderParameterLabel
  }

  showSelectedParameterType(typeName) {
    const type = getTypeFromList(this.parameterTypes, typeName)
    const $container = this.$typeOptionsContainer

    this.$group.empty()
    // Remove contents first before inserting elements for the selected parameter type
    $container.empty()
    
    this.type = type
    
    this.$group.append([
      this.$parameterName = this.renderParameterName(this, this.getInputName('name')),
      this.$parameterLabel = this.renderParameterLabel(this, this.getInputName('label'))
    ])

    $container.append(this.type.renderBuilder(this))

    return true
  }

  clearSelectedParameterType() {
    this.type = null
    this.$typeOptionsContainer.empty()
    this.$group.empty()
  }

  getInputName(key = '') {
    return `${this.inputName}[${this.index}]` + (key ? `[${key}]` : '')
  }

  getElement() {
    return this.$element
  }

  remove() {
    this.$element.remove()
  }

  render() {
    const $element = $(`<li class="${getClassName('list-item')}"></li>`)
    const $elementsGroup = $(`<div />`).addClass(getClassName('elements-group'))
 
    this.$group = $(`<div />`).addClass(getClassName('parameter-group'))

    this.$typeOptions = this.renderTypeOptions(this, this.parameterTypes)
    this.$typeOptionsContainer = $(`<div class="${getClassName('options')}"></div>`)

    // Add type selections, text input for the named parameter, and a container for the type's controls/elements
    this.$typeOptions.on('change', (evt) => {
      this.clearSelectedParameterType()

      if (evt.target.value) {
        this.showSelectedParameterType(evt.target.value)
        this.callbacks.fire('parameterChanged', this)
      }
    })
 
    $element
      .append(this.$typeOptions)
        .append($elementsGroup.append([
          this.$group,
          this.$typeOptionsContainer,
          this.renderRemoveButton($element)
        ])
      )

    this.$element = $element

    return this.$element
  }

  renderRemoveButton($parent) {
    this.$buttons = $(`<div class="${getClassName('buttons')}" />`)
    
    // Add remove button
    const $removeButton = this.$buttons
      .append(
        $('<button></button>')
        .addClass(getClassName('list-item-remove'))
        .attr('type', 'button')
        .text('Remove')
      )
  
    $parent.on('click', getSelector('list-item-remove'), (evt) => {
      evt.preventDefault()  
      this.manager.remove(this.index)
      this.remove()
    })

    return $removeButton
  }

  validate() {
    return this.type.validateBuilder(this)
  }

  setCallbacks(callbacks) {
    this.callbacks = callbacks

    return this
  }

  load(data) {
    this.showSelectedParameterType(data.type)

    this.$typeOptions.val(data.type)
    this.$parameterName.val(data.name)
    this.$parameterLabel.val(data.label)

    // Call the type's loadBuilder callback
    this.type.loadBuilder(this, data)
  }
}

export default TreeWalkerParameterBuilder