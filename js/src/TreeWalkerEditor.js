import CodeMirror from 'codemirror'
import sqlFormatter from 'sql-formatter'
import "codemirror/mode/sql/sql"
import "codemirror/addon/hint/sql-hint"
import 'codemirror/lib/codemirror.css'

export default class TreeWalkerEditor {
  constructor(selector, config) { 
    this.selector = selector
    this.format = this.sqlFormatter

    this.editor = CodeMirror.fromTextArea(document.querySelector(this.selector), {
      mode: "text/x-sql",
      theme: "default",
      lineNumbers: true,
      indentWithTabs : true,
      lineWrapping: true,
      smartIndent : true,
      matchBrackets: true,
      tabSize: 4,
      ...config
    })

  }

  /**
   * Format SQL code
   */
  sqlFormatter(value) {

    return sqlFormatter.format(value)
  }

}