import 'jstree/dist/jstree.min' 
import $ from 'jquery' 
 
export default class TreeWalker {
  constructor(selector, options = {}) {
    const self = this

    options.core = {
      dblclick_toggle : false,
      themes: {
        name: 'proton',
        responsive: true
      },
      ...options.core,
    }

    this.options = options
 
    const tree = $(selector).jstree(options)
 
    tree.on("hover_node.jstree", (event, data) => {
      const { node } = data

      if (options.node) {
        if (options.node.onHover && typeof options.node.onHover === 'function') {
          options.node.onHover(event, node)
        }
      }

    })
    
    tree.on('select_node.jstree', (event, data) => {
      const { node } = data

      if (options.node) {
        if (options.node.onClick && typeof options.node.onClick === 'function') {
          options.node.onClick(event, node)
        }
      }
    })

    tree.on("dblclick.jstree", function(event, data) {
      const instance = $.jstree.reference(this)
      const node = instance.get_node(event.target)

      if (options.node) {
        if (options.node.onDoubleClick && typeof options.node.onDoubleClick === 'function') {
          options.node.onDoubleClick(event, node)
        }
      }
    })

    tree.on("rename_node.jstree", (event, data) => {
      const { node } = data

      if (options.node) {
        if (options.node.onRename && typeof options.node.onRename === 'function') {
          options.node.onRename(event, node)
        }
      }
    })

    tree.on("create_node.jstree", (event, data) => {
      const { node } = data

      if (options.node) {
        if (options.node.onCreate && typeof options.node.onCreate === 'function') {
          options.node.onCreate(event, node)
        }
      }
    })

    if (options.search && options.search.selector) {
      $(options.search.selector).keyup(function() { 
        $(selector).jstree('search', $(this).val())
      })
    }
 
    return $.jstree.reference(selector)
  }

}