import $ from 'jquery'

export const bindMethods = (target, source, callbacks) => {
  // Bind all known callbacks to this instance
  callbacks.filter(method => typeof source[method] === 'function')
    .forEach(method => target[method] = source[method].bind(target))
}

export const callbacks = (events) => {
  const callbacks = events.reduce((result, event) => {
    return { ...result, [event]: $.Callbacks() }
  }, {})
  
  const listensTo = (event) => typeof callbacks[event] !== 'undefined'

  return {
    on: (event, fn) => {
      listensTo(event) && callbacks[event].add(event, fn)
    },
    off: (event, fn) => {
      listensTo(event) && callbacks[event].remove(event, fn)
    },
    fire: (event, ...args) => {
      listensTo(event) && callbacks[event].fire(...args)
    },
    getCallbackMap: () => callbacks 
  }
}