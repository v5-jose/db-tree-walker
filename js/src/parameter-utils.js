import $ from 'jquery'
import find from 'lodash/find'

export const getSelector = (str = '', prefix = '.') => prefix + 'treewalker-parameters' + (str ? `--${str}` : '')

export const getClassName = (str = '') => 'treewalker-parameters' + (str ? `--${str}` : '')

export const getTypeSelector = (type) => getSelector(`type-${type}`)

export const getTypeClassName = (type) => getClassName(`type-${type}`)

export const renderInput = (type) => $('<input />').addClass(getTypeClassName(type))

export const getTypeFromList = (types, name) => {
  let type = find(types, ['name', name])

  if (! type) {
    throw `Invalid parameter type: ${name}`
  }
  
  return type
}

export const renderLabeledElement = (parameter, ...$elements) => {
  const $wrapper = $('<div class="form-group"></div>')
  const $label = $('<label class="control-label"></label>')
  const $fieldWrapper = $('<div class="field-wrapper"></div>')

  return $wrapper.append([
    $label.append($('<span></span>').text(parameter.getLabel())),
    $fieldWrapper.append(...$elements)
  ])
}
