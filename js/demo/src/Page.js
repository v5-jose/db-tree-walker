import tippy from 'tippy.js'
import 'tippy.js/themes/light.css'
import { render, html } from 'lit-html'

import cookies from 'js-cookie'
import transformValues from './transformValues';
export default class Page {
 constructor() {
   this.init()
 }

/**
 * Render Tooltip for column
 * @param {*} evt
 * @param {*} selected 
 */
renderColumnTooltip(event, node) { 
  
  if (node.data.column) {
    const { parent, value: column, description, nickname, type } = node.data
     
    const template = `
      <div class="jstree-tooltip">
        <div class="jstree-tooltip__content-header"> 
            <label>${nickname}</label>
        </div> 
        <div class="jstree-tooltip__content">
          <div class="form-group">
            <label>Nickname Test</label>
            <div>${nickname}</div>
          </div>
          <div class="form-group">
            <label>Description</label>
            <div>${description}</div>
          </div> 
          <div class="form-group jstree-widget">
            <label>DB > Table > Column</label>
            <div class="jstree-widget__content">${parent.db}.${parent.table}.${column}</div>
          </div> 
          <div class="form-group jstree-widget">
            <label>Data Type</label>
            <div class="jstree-widget__content">${type}</div>
          </div>    
        </div>
      </div>
    `
   
    tippy(document.getElementById(node.id).querySelector('.jstree-anchor'), {
      arrow: true,
      animation: 'fade', 
      hideOnClick: true, 
      // trigger: 'click',
      interactive: true,
      content: template,
      animation: 'shift-toward',
      placement: 'right',
      theme: 'treewalker-light light', 
      delay: 300,
      maxWidth: 500
    })
  }
}

/**
 * Render modal for editing column's details
 * @param {*} evt 
 * @param {*} node 
 */
onEdit(evt, node, self) {
  const { parent, value: column, description, nickname, type } = node.data
  const $node_form = $("#node-form")

  self.tree_walker_modal.show('#tree-walker-modal_mm')
 
  const template = html`
    <div class="jstree-tooltip__content-header"> 
      <label>${nickname}</label>
    </div>
    <form id="node-form">
      <div class="form-group">
        <label>Nickname Test</label>
        <input type="text" class="form-control" placeholder="Nickname" name="nickname" value="${nickname}">
      </div>
      <div class="form-group">
        <label>Description</label>
        <input type="text" class="form-control" placeholder="description" name="description" value="${description}">
      </div> 
      <div class="form-group jstree-widget">
        <label>DB > Table > Column</label>
        <div class="jstree-widget__content">${parent.db}.${parent.table}.${column}</div>
      </div> 
      <div class="form-group jstree-widget">
        <label>Data Type</label>
        <div class="jstree-widget__content">${type}</div>
      </div> 
      <div class="form-group">
        <input type="button" value="Save" class="btn btn-primary pull-right" @click="${(e) => this.handleForm(e, node)}" />
      </div>
    </form>
  `

  render(template, document.querySelector('.content'))

}

handleForm(e, node) {
  const form = $("#node-form")

  this.data = this.data.map(db => {
    db.tables.map(table => {
      table.columns.map(column => {

        if (column.qualified_name === node.data.value) { 
          Object.assign(column, {
            name: form.find("input[name=nickname]").val()
          }) 
        }  

        return column
      })
      
      return table
    })

    return db
  })

 
  this.tree_walker.settings.core.data = this.data.map(transformValues)

  this.tree_walker.refresh(node.id)
}

/**
 * 
 * @param {*} evt 
 * @param {*} node 
 * @param {*} self 
 */
handleNodeDblClick(evt, node, self) {

  if (node.data.table || node.data.column) {
    const editor = self.code_mirror.editor
    const pos = editor.getCursor()

    editor.setSelection(pos, pos)
    editor.replaceSelection(node.data.value)
  }

}

/**
 * 
 * @param {*} editor 
 * @param {*} value 
 */
activeCodeMirrorTheme(editor, value) {
  editor.setOption("theme", value)
}

/**
 * Handle's page load
 */
onPageLoad() {
  const cm = this.code_mirror.editor
  const content = window.sessionStorage.getItem('__cm_content')

  if (content) { 
    cm.setValue(content)
  }
}
 
  
}