import { html, render } from 'lit-html'
import MicroModal from 'micromodal'

export default class Modal {

  constructor(selector) {
    this.selector = selector

    const template = html`
      <div class="modal" id='${this.selector}_mm' aria-hidden="true">
        <div class="modal__overlay" tabindex="-1" data-micromodal-close>
          <div class="modal__container" role="dialog">
            <div class="content"></div>
          </div>
        </div>
      </div>  
    `
    render(template, document.querySelector(this.selector))

    return MicroModal.init()
  }

  /**
   * Trigger the modal to show using modal id
   * @param {*} id 
   * @param {*} options 
   */
  show(id, options = {}) {
    
    MicroModal.show(id, {...options})
  }
  
}