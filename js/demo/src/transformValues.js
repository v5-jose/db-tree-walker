export default (db, index) => {
  db = {
    text: db.name,
    icon : "fas fa-database",
    data: {
      database: true
    },
    children: db.tables.map(table => {
      table = {
        text: table.name, 
        icon : "fas fa-table",  
        data: {
          value: table.qualified_name,
          table: true,
          parent: { 
            db: db.name, 
          }
        },
        children: table.columns.map(column => {
          column = {
            text: column.name, 
            icon : "fas fa-columns",
            type: 'column',
            data: {
              parent: { 
                db: db.name,
                table: table.name,
              },
              column: true,
              type: column.data_type,
              value: column.qualified_name,
              nickname: column.qualified_name,
              description: 'Sample'
            }
          }
          return column
        })
      }
      return table
    })
  } 
  return db
}