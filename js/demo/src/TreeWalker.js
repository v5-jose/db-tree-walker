import cookies from 'js-cookie'

import { TreeWalker, TreeWalkerEditor } from '../../src'
 
import Modal from './Modal'
import db from './db.json' 
import Page from './page'
import transformValues from './transformValues'

import '../../dist/styles/default.css'
import './styles/modal.css'
class TreeWalkerPage extends Page {

  init() {
    this.data = db.schemas
    this.schemas = this.data.map(transformValues)
 
    this.tree_walker = new TreeWalker("#tree-walker-demo", {
      core: {
        data: this.schemas,
        check_callback: true
      },
      plugins: ["state", "contextmenu", "search"],
      search: {
        case_insensitive: true,
        show_only_matches : true,
        selector: "#search"
      },
      contextmenu: {
        items: (node) => {
          let menu = {}

          if (node.data.column) {
            menu = {
              ...menu,
              Edit: {
                icon: "fas fa-pencil-alt",
                separator_before: false,
                separator_after: false,
                label: "Edit",
                action: (obj, event) => this.onEdit(event, node, this) 
              }
            }
          }

          if (node.data.table) {
            menu = {
              ...menu,
              Rename: {
                icon: "fas fa-pencil-alt",
                separator_before: false,
                separator_after: false,
                label: "Rename",
                action: (obj, event) => {
                  const ref = this.tree_walker.get_node(obj.reference)
 
                  this.tree_walker.edit(ref)
                }
              },
              Create: {
                icon: "fas fa-pencil-alt",
                separator_before: false,
                separator_after: false,
                label: "Create",
                action: (obj, evt) => {   
                  const ref = this.tree_walker.get_node(obj.reference)

                  this.tree_walker.create_node(ref, {}, "first",  (node) => {
                    console.log("new_node", node)
                    node.icon = "fas fa-columns"
                    node.data = { 
                      column: true
                    }

                    this.tree_walker.edit(node)
                  })
                }
              }
            }
          }

          return menu

        }
      },
      node: {
        onDoubleClick: (event, node) => this.handleNodeDblClick(event, node, this),
        onHover: (event, node) => this.renderColumnTooltip(event, node),
        onRename: (event, node) => console.log("rename", node),
        onCreate: (event, node) => console.log('create', node)
      }
    })

    this.$format_btn = $("#format-sql")
    this.$cm_theme = $("#select-theme")

    this.$active_theme = cookies.get('__editor_theme')
 
    this.code_mirror = new TreeWalkerEditor("#tree-walker-demo-textarea")
    this.tree_walker_modal = new Modal("#tree-walker-modal")

    this.code_mirror.editor.on("keyup", function(cm) {  
      window.sessionStorage.setItem('__cm_content', cm.getValue())
    })
 
    this.$format_btn.on('click', (e) => {
      const cm = this.code_mirror.editor
      const sql = this.code_mirror.format(cm.getValue())

      window.sessionStorage.setItem('__cm_content', sql)

      this.code_mirror.editor.setValue(sql) 
    })

    this.$cm_theme.on('change', (e) => { 
      this.activeCodeMirrorTheme(this.code_mirror.editor, e.target.value)
      cookies.set('__editor_theme', e.target.value)
    })

    if (this.$active_theme) {
      this.$cm_theme.val(this.$active_theme)
      this.activeCodeMirrorTheme(this.code_mirror.editor, this.$active_theme)
    } 

    this.onPageLoad()

  }

}

export default TreeWalkerPage