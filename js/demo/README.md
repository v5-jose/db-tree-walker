# Tree-Walker Demo Page
Tree Walker Demo page configured with babel, webpack and eslint.

## Usage
* `yarn start` to run eslint on watch mode and dev-server at localhost:8080.
* `yarn watch` to only watch for/recompile on changes.
* `yarn build` to generate a minified, production-ready build.
