'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _defaults = require('lodash/defaults');

var _defaults2 = _interopRequireDefault(_defaults);

var _TreeWalkerParameterBuilder = require('./TreeWalkerParameterBuilder');

var _TreeWalkerParameterBuilder2 = _interopRequireDefault(_TreeWalkerParameterBuilder);

var _utils = require('./utils');

var _parameterUtils = require('./parameter-utils');

var _defaults3 = require('./defaults');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var mergeDefaultOptions = function mergeDefaultOptions(options) {
  return (0, _defaults2.default)(options, {
    renderTypeOptions: _defaults3.renderTypeOptions,
    renderParameterName: _defaults3.renderParameterBuilderName,
    renderParameterLabel: _defaults3.renderParameterBuilderLabel
  });
};

var TreeWalkerParameterListBuilder = function () {
  function TreeWalkerParameterListBuilder(options) {
    _classCallCheck(this, TreeWalkerParameterListBuilder);

    options = mergeDefaultOptions(options);

    this.$element = (0, _jquery2.default)(options.element);
    this.inputName = options.inputName;
    this.parameterTypes = options.parameterTypes;
    this.parameters = [];
    this.nextIndex = 0;

    // Create callbacks for each known events and merge its methods to our instance
    Object.assign(this, (0, _utils.callbacks)(['parameterAdded', 'parameterChanged']));

    (0, _utils.bindMethods)(this, options, ['renderTypeOptions', 'renderParameterName', 'renderParameterLabel']);
  }

  _createClass(TreeWalkerParameterListBuilder, [{
    key: 'add',
    value: function add() {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      var parameter = new _TreeWalkerParameterBuilder2.default(this, {
        parameterTypes: this.parameterTypes,
        inputName: this.inputName,
        index: this.nextIndex,
        renderParameterName: this.renderParameterName,
        renderTypeOptions: this.renderTypeOptions,
        renderParameterLabel: this.renderParameterLabel
      });

      this.parameters.push(parameter);
      // Use our callbacks to fire events
      parameter.setCallbacks(this);

      this.$element.find((0, _parameterUtils.getSelector)('list')).append(parameter.render());
      this.nextIndex++;

      // Load if initial data was specified
      if (data) {
        parameter.load(data);
      }

      this.fire('parameterAdded', parameter);

      return parameter;
    }
  }, {
    key: 'remove',
    value: function remove(index) {
      if (typeof this.parameters[index] !== 'undefined') {
        var parameter = this.parameters[index];
        this.parameters.splice(index, 1);

        return parameter;
      }

      return false;
    }
  }, {
    key: 'validate',
    value: function validate() {
      var valid = false;

      for (var i = 0; i < this.parameters.length; i++) {
        var parameter = this.parameters[i];
        valid = valid && parameter.validate();
      }

      return valid;
    }
  }, {
    key: 'count',
    value: function count() {
      return this.parameters.length;
    }
  }, {
    key: 'load',
    value: function load(parameters) {
      var _this = this;

      parameters.forEach(function (parameter) {
        return _this.add(parameter);
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      this.$element.html('\n    <div class="' + (0, _parameterUtils.getClassName)('wrapper') + '">\n      <ul class="' + (0, _parameterUtils.getClassName)('list') + '"></ul>\n      <button class="btn btn-primary ' + (0, _parameterUtils.getClassName)('add') + '" type="button">Add Parameter</button>\n    </div>\n    ');

      this.$element.on('click', (0, _parameterUtils.getSelector)('add'), function (evt) {
        evt.preventDefault();
        _this2.add();
      });
    }
  }, {
    key: 'getElement',
    value: function getElement() {
      return this.$element;
    }
  }]);

  return TreeWalkerParameterListBuilder;
}();

exports.default = TreeWalkerParameterListBuilder;