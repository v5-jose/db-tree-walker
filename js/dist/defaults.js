'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderTypeOptions = renderTypeOptions;
exports.renderParameterBuilderName = renderParameterBuilderName;
exports.renderParameterBuilderLabel = renderParameterBuilderLabel;

var _parameterUtils = require('./parameter-utils');

function renderTypeOptions(parameter, types) {
  var $select = $('<select class="' + (0, _parameterUtils.getClassName)('type-select') + '"></select>');

  $select.attr('name', parameter.getInputName('type'));
  $select.append('<option value="">--Select Type--</option>');

  $select.append(types.map(function (type) {
    return $('<option></option>').val(type.name).text(type.label);
  }));

  return $select;
}

function renderParameterBuilderName(parameter, inputName) {
  return $('<input />').attr({
    type: 'text',
    name: inputName,
    placeholder: "Enter parameter name"
  }).addClass((0, _parameterUtils.getClassName)('parameter-name-input'));
}

function renderParameterBuilderLabel(parameter, inputName) {
  return $('<input />').attr({
    type: 'text',
    name: inputName,
    placeholder: "Enter parameter label"
  }).addClass((0, _parameterUtils.getClassName)('parameter-label-input'));
}