'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.callbacks = exports.bindMethods = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var bindMethods = exports.bindMethods = function bindMethods(target, source, callbacks) {
  // Bind all known callbacks to this instance
  callbacks.filter(function (method) {
    return typeof source[method] === 'function';
  }).forEach(function (method) {
    return target[method] = source[method].bind(target);
  });
};

var callbacks = exports.callbacks = function callbacks(events) {
  var callbacks = events.reduce(function (result, event) {
    return _extends({}, result, _defineProperty({}, event, _jquery2.default.Callbacks()));
  }, {});

  var listensTo = function listensTo(event) {
    return typeof callbacks[event] !== 'undefined';
  };

  return {
    on: function on(event, fn) {
      listensTo(event) && callbacks[event].add(event, fn);
    },
    off: function off(event, fn) {
      listensTo(event) && callbacks[event].remove(event, fn);
    },
    fire: function fire(event) {
      var _callbacks$event;

      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      listensTo(event) && (_callbacks$event = callbacks[event]).fire.apply(_callbacks$event, args);
    },
    getCallbackMap: function getCallbackMap() {
      return callbacks;
    }
  };
};