'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

require('jstree/dist/jstree.min');

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TreeWalker = function TreeWalker(selector) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  _classCallCheck(this, TreeWalker);

  var self = this;

  options.core = _extends({
    dblclick_toggle: false,
    themes: {
      name: 'proton',
      responsive: true
    }
  }, options.core);

  this.options = options;

  var tree = (0, _jquery2.default)(selector).jstree(options);

  tree.on("hover_node.jstree", function (event, data) {
    var node = data.node;


    if (options.node) {
      if (options.node.onHover && typeof options.node.onHover === 'function') {
        options.node.onHover(event, node);
      }
    }
  });

  tree.on('select_node.jstree', function (event, data) {
    var node = data.node;


    if (options.node) {
      if (options.node.onClick && typeof options.node.onClick === 'function') {
        options.node.onClick(event, node);
      }
    }
  });

  tree.on("dblclick.jstree", function (event, data) {
    var instance = _jquery2.default.jstree.reference(this);
    var node = instance.get_node(event.target);

    if (options.node) {
      if (options.node.onDoubleClick && typeof options.node.onDoubleClick === 'function') {
        options.node.onDoubleClick(event, node);
      }
    }
  });

  tree.on("rename_node.jstree", function (event, data) {
    var node = data.node;


    if (options.node) {
      if (options.node.onRename && typeof options.node.onRename === 'function') {
        options.node.onRename(event, node);
      }
    }
  });

  tree.on("create_node.jstree", function (event, data) {
    var node = data.node;


    if (options.node) {
      if (options.node.onCreate && typeof options.node.onCreate === 'function') {
        options.node.onCreate(event, node);
      }
    }
  });

  if (options.search && options.search.selector) {
    (0, _jquery2.default)(options.search.selector).keyup(function () {
      (0, _jquery2.default)(selector).jstree('search', (0, _jquery2.default)(this).val());
    });
  }

  return _jquery2.default.jstree.reference(selector);
};

exports.default = TreeWalker;