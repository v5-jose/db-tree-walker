'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _defaults = require('lodash/defaults');

var _defaults2 = _interopRequireDefault(_defaults);

var _parameterUtils = require('./parameter-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var mergeDefaultOptions = function mergeDefaultOptions(options) {
  return (0, _defaults2.default)(options, {
    config: {}
  });
};

var TreeWalkerParameterBuilder = function () {
  function TreeWalkerParameterBuilder(manager, options) {
    _classCallCheck(this, TreeWalkerParameterBuilder);

    this.manager = manager;
    options = mergeDefaultOptions(options);

    this.parameterTypes = options.parameterTypes;
    this.inputName = options.inputName;
    this.index = options.index;
    this.config = options.config;
    this.renderTypeOptions = options.renderTypeOptions;
    this.renderParameterName = options.renderParameterName;
    this.renderParameterLabel = options.renderParameterLabel;
  }

  _createClass(TreeWalkerParameterBuilder, [{
    key: 'showSelectedParameterType',
    value: function showSelectedParameterType(typeName) {
      var type = (0, _parameterUtils.getTypeFromList)(this.parameterTypes, typeName);
      var $container = this.$typeOptionsContainer;

      this.$group.empty();
      // Remove contents first before inserting elements for the selected parameter type
      $container.empty();

      this.type = type;

      this.$group.append([this.$parameterName = this.renderParameterName(this, this.getInputName('name')), this.$parameterLabel = this.renderParameterLabel(this, this.getInputName('label'))]);

      $container.append(this.type.renderBuilder(this));

      return true;
    }
  }, {
    key: 'clearSelectedParameterType',
    value: function clearSelectedParameterType() {
      this.type = null;
      this.$typeOptionsContainer.empty();
      this.$group.empty();
    }
  }, {
    key: 'getInputName',
    value: function getInputName() {
      var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

      return this.inputName + '[' + this.index + ']' + (key ? '[' + key + ']' : '');
    }
  }, {
    key: 'getElement',
    value: function getElement() {
      return this.$element;
    }
  }, {
    key: 'remove',
    value: function remove() {
      this.$element.remove();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this = this;

      var $element = (0, _jquery2.default)('<li class="' + (0, _parameterUtils.getClassName)('list-item') + '"></li>');
      var $elementsGroup = (0, _jquery2.default)('<div />').addClass((0, _parameterUtils.getClassName)('elements-group'));

      this.$group = (0, _jquery2.default)('<div />').addClass((0, _parameterUtils.getClassName)('parameter-group'));

      this.$typeOptions = this.renderTypeOptions(this, this.parameterTypes);
      this.$typeOptionsContainer = (0, _jquery2.default)('<div class="' + (0, _parameterUtils.getClassName)('options') + '"></div>');

      // Add type selections, text input for the named parameter, and a container for the type's controls/elements
      this.$typeOptions.on('change', function (evt) {
        _this.clearSelectedParameterType();

        if (evt.target.value) {
          _this.showSelectedParameterType(evt.target.value);
          _this.callbacks.fire('parameterChanged', _this);
        }
      });

      $element.append(this.$typeOptions).append($elementsGroup.append([this.$group, this.$typeOptionsContainer, this.renderRemoveButton($element)]));

      this.$element = $element;

      return this.$element;
    }
  }, {
    key: 'renderRemoveButton',
    value: function renderRemoveButton($parent) {
      var _this2 = this;

      this.$buttons = (0, _jquery2.default)('<div class="' + (0, _parameterUtils.getClassName)('buttons') + '" />');

      // Add remove button
      var $removeButton = this.$buttons.append((0, _jquery2.default)('<button></button>').addClass((0, _parameterUtils.getClassName)('list-item-remove')).attr('type', 'button').text('Remove'));

      $parent.on('click', (0, _parameterUtils.getSelector)('list-item-remove'), function (evt) {
        evt.preventDefault();
        _this2.manager.remove(_this2.index);
        _this2.remove();
      });

      return $removeButton;
    }
  }, {
    key: 'validate',
    value: function validate() {
      return this.type.validateBuilder(this);
    }
  }, {
    key: 'setCallbacks',
    value: function setCallbacks(callbacks) {
      this.callbacks = callbacks;

      return this;
    }
  }, {
    key: 'load',
    value: function load(data) {
      this.showSelectedParameterType(data.type);

      this.$typeOptions.val(data.type);
      this.$parameterName.val(data.name);
      this.$parameterLabel.val(data.label);

      // Call the type's loadBuilder callback
      this.type.loadBuilder(this, data);
    }
  }]);

  return TreeWalkerParameterBuilder;
}();

exports.default = TreeWalkerParameterBuilder;