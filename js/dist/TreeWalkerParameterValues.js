'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TreeWalkerParameterValues = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _find = require('lodash/find');

var _find2 = _interopRequireDefault(_find);

var _parameterTypes = require('./parameter-types');

var _parameterTypes2 = _interopRequireDefault(_parameterTypes);

var _TreeWalkerParameter = require('./TreeWalkerParameter');

var _TreeWalkerParameter2 = _interopRequireDefault(_TreeWalkerParameter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TreeWalkerParameterValues = exports.TreeWalkerParameterValues = function () {
  function TreeWalkerParameterValues(element) {
    var parameterTypes = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _parameterTypes2.default;
    var parameterConfigs = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

    _classCallCheck(this, TreeWalkerParameterValues);

    this.$element = (0, _jquery2.default)(element);
    this.parameterTypes = parameterTypes;
    this.setParameterConfigs(parameterConfigs);
  }

  _createClass(TreeWalkerParameterValues, [{
    key: 'setParameterConfigs',
    value: function setParameterConfigs(parameterConfigs) {
      var _this = this;

      this.parameters = parameterConfigs.map(function (config) {
        return _this.buildParameter(config);
      });

      return this;
    }
  }, {
    key: 'buildParameter',
    value: function buildParameter(config) {
      var Type = (0, _find2.default)(this.parameterTypes, config.type);

      if (!Type) {
        throw invalidParameterTypeErr(config.type);
      }

      return new _TreeWalkerParameter2.default();
    }
  }, {
    key: 'render',
    value: function render() {
      for (var i = 0; i < this.parameters.length; i++) {
        this.$element.append();
      }
    }
  }]);

  return TreeWalkerParameterValues;
}();