'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _codemirror = require('codemirror');

var _codemirror2 = _interopRequireDefault(_codemirror);

var _sqlFormatter2 = require('sql-formatter');

var _sqlFormatter3 = _interopRequireDefault(_sqlFormatter2);

require('codemirror/mode/sql/sql');

require('codemirror/addon/hint/sql-hint');

require('codemirror/lib/codemirror.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TreeWalkerEditor = function () {
  function TreeWalkerEditor(selector, config) {
    _classCallCheck(this, TreeWalkerEditor);

    this.selector = selector;
    this.format = this.sqlFormatter;

    this.editor = _codemirror2.default.fromTextArea(document.querySelector(this.selector), _extends({
      mode: "text/x-sql",
      theme: "default",
      lineNumbers: true,
      indentWithTabs: true,
      lineWrapping: true,
      smartIndent: true,
      matchBrackets: true,
      tabSize: 4
    }, config));
  }

  /**
   * Format SQL code
   */


  _createClass(TreeWalkerEditor, [{
    key: 'sqlFormatter',
    value: function sqlFormatter(value) {

      return _sqlFormatter3.default.format(value);
    }
  }]);

  return TreeWalkerEditor;
}();

exports.default = TreeWalkerEditor;