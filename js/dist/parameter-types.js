'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createNumber = exports.createText = exports.createType = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _noop = require('lodash/noop');

var _noop2 = _interopRequireDefault(_noop);

var _parameterUtils = require('./parameter-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createType = exports.createType = function createType(name, label, props) {
  return _extends({
    name: name,
    label: label,
    validateBuilder: function validateBuilder() {
      return false;
    },
    loadBuilder: _noop2.default,
    load: _noop2.default,
    setValue: function setValue(parameter, value) {
      return value || parameter.getDefaultValue();
    },
    getValue: function getValue(parameter, value) {
      return value || parameter.getDefaultValue();
    }
  }, props);
};

var createText = exports.createText = function createText() {
  var renderInput = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _parameterUtils.renderInput;

  var TYPE_TEXT = 'string';
  var createTextInput = function createTextInput() {
    return renderInput(TYPE_TEXT).attr('type', 'text');
  };

  return createType(TYPE_TEXT, 'Text', {
    loadBuilder: function loadBuilder(parameter, config) {
      parameter.getElement().find((0, _parameterUtils.getTypeSelector)(TYPE_TEXT)).val(config.default);
    },
    renderBuilder: function renderBuilder(parameter) {
      return createTextInput().attr('name', parameter.getInputName('default')).attr('placeholder', 'Enter default value');
    },
    setValue: function setValue(parameter, value) {
      parameter.getElement().find(':input[name="' + parameter.getInputName('value') + '"]').val(value);

      return value;
    },
    render: function render(parameter) {
      return (0, _parameterUtils.renderLabeledElement)(parameter, [parameter.renderParameterNameHiddenInput(), createTextInput().attr('name', parameter.getInputName('value')).val(parameter.getValue()).addClass('form-control')]);
    }
  });
};

var createNumber = exports.createNumber = function createNumber() {
  var renderInput = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _parameterUtils.renderInput;

  var TYPE_NUMBER = 'number';
  var createNumberInput = function createNumberInput() {
    return renderInput(TYPE_NUMBER).attr('type', 'number');
  };

  return createType(TYPE_NUMBER, 'Number', {
    loadBuilder: function loadBuilder(parameter, config) {
      parameter.getElement().find((0, _parameterUtils.getTypeSelector)(TYPE_NUMBER)).val(config.default);
    },
    renderBuilder: function renderBuilder(parameter) {
      return $('<div />').append([createNumberInput().attr('name', parameter.getInputName('default')).attr('placeholder', 'Enter default value')]);
    },
    setValue: function setValue(parameter, value) {
      parameter.getElement().find(':input[name="' + parameter.getInputName('value') + '"]').val(value);

      return value;
    },
    render: function render(parameter) {
      return (0, _parameterUtils.renderLabeledElement)(parameter, [parameter.renderParameterNameHiddenInput(), createNumberInput().attr('name', parameter.getInputName('value')).val(parameter.getValue()).addClass('form-control')]);
    }
  });
};