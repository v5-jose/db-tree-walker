'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _defaults = require('lodash/defaults');

var _defaults2 = _interopRequireDefault(_defaults);

var _find = require('lodash/find');

var _find2 = _interopRequireDefault(_find);

var _TreeWalkerParameter = require('./TreeWalkerParameter');

var _TreeWalkerParameter2 = _interopRequireDefault(_TreeWalkerParameter);

var _parameterUtils = require('./parameter-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var mergeDefaultOptions = function mergeDefaultOptions(options) {
  return (0, _defaults2.default)(options, {
    inputName: 'treewalker_parameters',
    values: {}
  });
};

var TreeWalkerParameterList = function () {
  function TreeWalkerParameterList(options) {
    _classCallCheck(this, TreeWalkerParameterList);

    options = mergeDefaultOptions(options);

    this.$element = (0, _jquery2.default)(options.element);
    this.$title = (0, _jquery2.default)(options.title);
    this.parameterTypes = options.parameterTypes;
    this.inputName = options.inputName;
    this.parameters = [];
    this.$wrapper = (0, _jquery2.default)('<ul></ul>').addClass((0, _parameterUtils.getClassName)('list'));

    if (options.parameters instanceof Array) {
      this.loadParameters(options.parameters, options.values);
    }
  }

  /**
   * 
   * @param {Array} parameters
   * @param {Array} values
   * @return {void} 
   */


  _createClass(TreeWalkerParameterList, [{
    key: 'loadParameters',
    value: function loadParameters(parameters, values) {
      var _this = this;

      this.parameters = parameters.map(function (config, index) {
        return new _TreeWalkerParameter2.default({
          type: (0, _parameterUtils.getTypeFromList)(_this.parameterTypes, config.type),
          inputName: _this.inputName,
          value: typeof values[config.name] !== 'undefined' ? values[config.name] : null,
          config: config,
          index: index
        });
      });

      return this;
    }
  }, {
    key: 'getParameterByName',
    value: function getParameterByName(name) {
      return (0, _find2.default)(this.parameters, function (parameter) {
        return parameter.getName() === name;
      });
    }
  }, {
    key: 'setValues',
    value: function setValues(values) {
      var _this2 = this;

      if (this.parameters.length > 0) {
        values.forEach(function (value) {
          var parameter = _this2.getParameterByName(value.name);
          if (parameter) {
            parameter.setValue(value.value);
          }
        });
      }

      return this;
    }
  }, {
    key: 'render',
    value: function render() {
      this.$element.append([this.$title, this.$wrapper.append(this.parameters.map(function (parameter) {
        return parameter.render();
      }))]);
    }
  }]);

  return TreeWalkerParameterList;
}();

exports.default = TreeWalkerParameterList;