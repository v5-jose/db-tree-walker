'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _defaults = require('lodash/defaults');

var _defaults2 = _interopRequireDefault(_defaults);

var _parameterUtils = require('./parameter-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var mergeDefaultOptions = function mergeDefaultOptions(options) {
  return (0, _defaults2.default)(options, {
    value: null
  });
};

var TreeWalkerParameter = function () {
  function TreeWalkerParameter(options) {
    _classCallCheck(this, TreeWalkerParameter);

    options = mergeDefaultOptions(options);

    this.type = options.type;
    this.inputName = options.inputName;
    this.config = options.config;
    this.value = options.value;
    this.index = options.index;
  }

  _createClass(TreeWalkerParameter, [{
    key: 'getInputName',
    value: function getInputName() {
      var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

      return this.inputName + '[' + this.index + ']' + (key ? '[' + key + ']' : '');
    }
  }, {
    key: 'getLabel',
    value: function getLabel() {
      return this.config.label;
    }
  }, {
    key: 'getDefaultValue',
    value: function getDefaultValue() {
      return this.config.default;
    }
  }, {
    key: 'setValue',
    value: function setValue(value) {
      this.value = this.type.setValue(this, value);

      return this;
    }
  }, {
    key: 'getValue',
    value: function getValue() {
      return this.type.getValue(this, this.value);
    }
  }, {
    key: 'getName',
    value: function getName() {
      return this.config.name;
    }
  }, {
    key: 'renderParameterNameHiddenInput',
    value: function renderParameterNameHiddenInput() {
      return (0, _jquery2.default)('<input />').attr({
        type: 'hidden',
        name: this.getInputName('name')
      }).val(this.config.name);
    }
  }, {
    key: 'getElement',
    value: function getElement() {
      if (!this.$element) {
        this.$element = (0, _jquery2.default)('<li></li>').addClass((0, _parameterUtils.getClassName)('list-item')).addClass((0, _parameterUtils.getClassName)('list-item-' + this.type.name)).append(this.type.render(this));
      }

      return this.$element;
    }
  }, {
    key: 'render',
    value: function render() {
      return this.getElement();
    }
  }]);

  return TreeWalkerParameter;
}();

exports.default = TreeWalkerParameter;