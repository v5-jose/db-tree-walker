'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _defaults = require('lodash/defaults');

var _defaults2 = _interopRequireDefault(_defaults);

var _TreeWalkerParameter = require('./TreeWalkerParameter');

var _TreeWalkerParameter2 = _interopRequireDefault(_TreeWalkerParameter);

var _parameterUtils = require('./parameter-utils');

var _defaults3 = require('./defaults');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var mergeDefaultOptions = function mergeDefaultOptions(options) {
  return (0, _defaults2.default)(options, {
    renderParameterName: _defaults3.renderParameterName,
    renderTypeOptions: _defaults3.renderTypeOptions
  });
};

var TreeWalkerParameterManager = function () {
  function TreeWalkerParameterManager(options) {
    var _this = this;

    _classCallCheck(this, TreeWalkerParameterManager);

    options = mergeDefaultOptions(options);

    this.$element = (0, _jquery2.default)(options.element);
    this.inputName = options.inputName;
    this.parameterTypes = options.parameterTypes;
    this.callbacks = _jquery2.default.Callbacks();
    this.parameters = [];

    var methods = ['renderTypeOptions', 'renderParameterName'];
    // Bind all known callbacks to this instance
    methods.forEach(function (method) {
      if (typeof options[method] === 'function') {
        _this[method] = options[method].bind(_this);
      }
    });
  }

  _createClass(TreeWalkerParameterManager, [{
    key: 'add',
    value: function add() {
      var parameter = new _TreeWalkerParameter2.default({
        parameterTypes: this.parameterTypes,
        inputName: this.inputName,
        index: this.parameters.length,
        renderParameterName: this.renderParameterName,
        renderTypeOptions: this.renderTypeOptions
      });

      this.parameters.push(parameter);

      parameter.setCallbacks(this.callbacks).render(this.$element.find((0, _parameterUtils.getSelector)('list')));

      this.callbacks.fire('parameterAdded', parameter);

      return parameter;
    }
  }, {
    key: 'remove',
    value: function remove(index) {
      if (typeof this.parameters[index] !== 'undefined') {
        return this.parameters.splice(index, 1)[0];
      }

      return false;
    }
  }, {
    key: 'validate',
    value: function validate() {
      var valid = false;

      for (var i = 0; i < this.parameters.length; i++) {
        var parameter = this.parameters[i];
        valid = valid && parameter.validate();
      }

      return valid;
    }
  }, {
    key: 'count',
    value: function count() {
      return this.parameters.length;
    }
  }, {
    key: 'load',
    value: function load(parameters) {
      var _this2 = this;

      parameters.forEach(function (parameter) {
        _this2.add().load(parameter);
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      this.$element.html('\n      <ul class="treewalker-parameters-list"></ul>\n      <button class="btn ' + (0, _parameterUtils.getClassName)('add') + '" type="button">Add Parameter</button>\n    ');

      this.$element.on('click', (0, _parameterUtils.getSelector)('add'), function (evt) {
        evt.preventDefault();
        _this3.add();
      });
    }
  }]);

  return TreeWalkerParameterManager;
}();

exports.default = TreeWalkerParameterManager;