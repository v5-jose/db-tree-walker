'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderLabeledElement = exports.getTypeFromList = exports.renderInput = exports.getTypeClassName = exports.getTypeSelector = exports.getClassName = exports.getSelector = undefined;

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _find = require('lodash/find');

var _find2 = _interopRequireDefault(_find);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getSelector = exports.getSelector = function getSelector() {
  var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var prefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '.';
  return prefix + 'treewalker-parameters' + (str ? '--' + str : '');
};

var getClassName = exports.getClassName = function getClassName() {
  var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  return 'treewalker-parameters' + (str ? '--' + str : '');
};

var getTypeSelector = exports.getTypeSelector = function getTypeSelector(type) {
  return getSelector('type-' + type);
};

var getTypeClassName = exports.getTypeClassName = function getTypeClassName(type) {
  return getClassName('type-' + type);
};

var renderInput = exports.renderInput = function renderInput(type) {
  return (0, _jquery2.default)('<input />').addClass(getTypeClassName(type));
};

var getTypeFromList = exports.getTypeFromList = function getTypeFromList(types, name) {
  var type = (0, _find2.default)(types, ['name', name]);

  if (!type) {
    throw 'Invalid parameter type: ' + name;
  }

  return type;
};

var renderLabeledElement = exports.renderLabeledElement = function renderLabeledElement(parameter) {
  for (var _len = arguments.length, $elements = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    $elements[_key - 1] = arguments[_key];
  }

  var $wrapper = (0, _jquery2.default)('<div class="form-group"></div>');
  var $label = (0, _jquery2.default)('<label class="control-label"></label>');
  var $fieldWrapper = (0, _jquery2.default)('<div class="field-wrapper"></div>');

  return $wrapper.append([$label.append((0, _jquery2.default)('<span></span>').text(parameter.getLabel())), $fieldWrapper.append.apply($fieldWrapper, $elements)]);
};