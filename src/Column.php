<?php

namespace Vector5\DB\TreeWalker;

use Illuminate\Contracts\Support\Arrayable;

class Column implements Arrayable
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var \Vector5\DB\TreeWalker\Table 
     */
    protected $table;

    /**
     * @var string
     */
    protected $dataType;

    /**
     * @var boolean
     */
    protected $isPrimaryKey;

    /**
     * @var string
     */
    protected $references;

    /**
     * @param \Vector5\DB\TreeWalker\Table $table
     * @param string $name
     * @param string $dataType
     */
    public function __construct(Table $table, $name, $dataType, $primaryKey = false)
    {
        $this->table = $table;
        $this->name = $name;
        $this->dataType = $dataType;
        $this->isPrimaryKey = $primaryKey;
    }

    /**
     * Get the name of the column
     * 
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the table this column belongs to.
     * 
     * @return \Vector5\DB\TreeWalker\Table 
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Get the column's data type.
     * 
     * @return string
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * Get the column's primary key flag.
     * 
     * @return boolean
     */
    public function getIsPrimaryKey()
    {
        return $this->isPrimaryKey;
    }

    /**
     * Set the column's primary key flag.
     * 
     * @param boolean $value
     */
    public function setIsPrimaryKey($value)
    {
        $this->isPrimaryKey = $value;
    }

    /**
     * Check if the column is a foreign key.
     *
     * @return bool
     */
    public function isForeignKey()
    {
        return ! empty($this->references);
    }

    /**
     * Set the references column of the column.
     *
     * @param string|\Vector5\DB\TreeWalker\Column $column
     * @param string|\Vector5\DB\TreeWalker\Table $table (optional)
     * @return static
     */
    public function references($column, $table = null)
    {
        if ($column instanceof self) {
            $this->setForeignKeyReferenceValue($column->getQualifiedName());
        } else {
            if (! $table instanceof Table) {
                $table = new Table($this->table->getSchema(), $table);
            }

            $this->setForeignKeyReferenceValue($table->qualifyColumn($column));
        }

        return $this;
    }

    /**
     * Set the foreign key referenced column.
     *
     * @param string $value
     * @return self
     */
    protected function setForeignKeyReferenceValue($value)
    {
        $this->references = $value;

        return $this;
    }

    /**
     * Get the qualified referenced column.
     *
     * @return string|null
     */
    public function getQualifiedReferencedColumn()
    {
        if ($this->isForeignKey()) {
            return $this->references;
        }

        return null;
    }

    /**
     * Get the fully qualified column name.
     * 
     * @return string
     */
    public function getQualifiedName()
    {
        return $this->table->getName().'.'.$this->getName();
    }

    /**
     * Get the column's array representation.
     * 
     * @return array
     */
    public function toArray()
    {
        return [
            'name' => $this->getName(),
            'table' => $this->table->getName(),
            'data_type' => $this->getDataType(),
            'qualified_name' => $this->getQualifiedName(),
            'is_primary_key' => $this->getIsPrimaryKey(),
            'is_foreign_key' => $this->isForeignKey(),
            'references' => $this->getQualifiedReferencedColumn()
        ];
    }
}
