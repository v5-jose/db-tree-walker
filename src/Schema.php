<?php

namespace Vector5\DB\TreeWalker;

use Vector5\DB\TreeWalker\Revisions\RevisionsIdentifier;
use Vector5\DB\TreeWalker\Revisions\StandardRevisionsIdentifier;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Arrayable;
use InvalidArgumentException;

class Schema implements Arrayable
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $tables;

    /**
     * @var \Vector5\DB\Revisions\RevisionsIdentifier
     */
    protected $revisionsIdentifier;

    /**
     * @param string $name
     * @param array|\Vector5\DB\TreeWalker\Table[] $tables (optional)
     */
    public function __construct($name, array $tables = [])
    {
        $this->name = $name;

        $this->setTables($tables);
    }

    /**
     * Get the schema name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add a table given its details
     * 
     * @param string|\Vector5\DB\TreeWalker\Table $table
     * @param array $columns (optional)
     * @return \Vector5\DB\TreeWalker\Table
     */
    public function addTable($table, array $columns = [])
    {
        // Make sure the column was built by the table's schema
        if ($table instanceof Table) {
            if ($table->getSchema() !== $this) {
                throw new InvalidArgumentException('Treewalker: Cannot add table from another schema');
            }
        } else  {
            $table = $this->buildTable($table, $columns);
        }

        $this->tables->put($table->getName(), $table);

        return $table;
    }

    /**
     * Build a new table instance for the schema
     * 
     * @param string $name
     * @param array $columns (optional)
     * @return \Vector5\DB\TreeWalker\Table
     */
    public function buildTable($name, array $columns = [])
    {
        return new Table($this, $name, $columns);
    }

    /**
     * Get the table given its name
     * 
     * @param string $name
     * @return \Vector5\DB\TreeWalker\Table
     */
    public function getTable($name)
    {
        return $this->tables->get($name);
    }

    /**
     * Get the list of tables.
     *
     * @return \Vector5\DB\TreeWalker\Table[]
     */
    public function getTables()
    {
        return $this->tables->all();
    }

    /**
     * Get the underlying tables collection
     * 
     * @return \Illuminate\Support\Collection
     */
    public function getTableCollection()
    {
        return $this->tables;
    }

    /**
     * Set the schema's table
     * 
     * @param array $tables
     * @return self
     */
    public function setTables(array $tables)
    {
        $this->tables = new Collection();

        foreach ($tables as $table) {
            if ($table instanceof Table) {
                $this->addTable($table);
            } else {
                $this->addTable($table['name'], $table['columns'] ?? []);
            }
        }

        return $this;
    }

    /**
     * Check if the schema has any table
     * 
     * @return bool
     */
    public function hasTables()
    {
        return $this->tables->isNotEmpty();
    }

    /**
     * Remove table from the list
     *
     * @param Table $table
     * @return void
     */
    public function removeTable($table)
    {
        if ($table instanceof Table) {
            if ($this === $table->getSchema()) {
                $this->tables->forget($table->getName());
            }
        } elseif (is_string($table)) {
            $this->tables->forget($table);
        }
    }

    /**
     * Get the revisions/changes of the schema with the given one.
     * 
     * @param \Vector5\DB\TreeWalker\Schema $otherSchema
     * @return array
     */
    public function revisions(Schema $otherSchema)
    {
        return $this->getRevisionsIdentifier()->revisions($this, $otherSchema);    
    }

    /**
     * Set the revisions identifier for the schema
     * 
     * @param \Vector5\DB\TreeWalker\Revisions\RevisionsIdentifier $identifier
     * @return self
     */
    public function setRevisionsIdentifier(RevisionsIdentifier $identifier)
    {
        $this->revisionsIdentifier = $identifier; 

        return self;
    }

    /**
     * Get the revisions identifier used by the schema
     * 
     * @return \Vector5\DB\TreeWalker\Revisions\RevisionsIdentifier
     */
    public function getRevisionsIdentifier()
    {
        return $this->revisionsIdentifier ?? ($this->revisionsIdentifier = new StandardRevisionsIdentifier());
    }

    /**
     * Get the array presentation of the schema model.
     * 
     * @return array
     */
    public function toArray()
    {
        return [
            'name' => $this->getName(),
            'tables' => $this->tables->values()->toArray(),
            'table_count' => $this->tables->count()
        ];
    }

    /**
     * Copy table into the list
     *
     * @param Table $table
     * @return Table
     */
    public function copyTable(Table $table)
    {
        $ownTable = $this->addTable($table->getName());
        
        foreach ($table->getColumns() as $column) {
            $ownTable->copyColumn($column);
        }

        return $ownTable;
    }
}