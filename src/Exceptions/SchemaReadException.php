<?php

namespace Vector5\DB\TreeWalker\Exceptions;

class SchemaReadException extends TreeWalkerException
{
    /**
     * @param string $schema
     */
    public function __construct($schema)
    {
        parent::__construct(sprintf(
            "Cannot read database schema %s", $schema
        ));
    }
}