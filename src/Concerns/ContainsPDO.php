<?php

namespace Vector5\DB\TreeWalker\Concerns;

use PDO;

trait ContainsPDO
{
    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * Get the PDO instance used by the tree walker.
     * 
     * @return \PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }
    
    /**
     * Set the PDO instance used by the tree walker
     * 
     * @return self
     */
    public function setPdo(PDO $pdo)
    {
        $this->pdo = $pdo;

        return $this;
    }
}