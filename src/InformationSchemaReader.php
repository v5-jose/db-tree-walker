<?php

namespace Vector5\DB\TreeWalker;

use Vector5\DB\TreeWalker\Schema;
use Vector5\DB\TreeWalker\Exceptions\SchemaReadException;
use Illuminate\Support\Collection;
use Exception;

abstract class InformationSchemaReader
{
    /**
     * @var string
     */
    protected $table = 'information_schema.columns';

    /**
     * @var string
     */
    protected $colTable = 'TABLE_NAME',
              $colSchema = 'TABLE_SCHEMA',
              $colCatalog = 'TABLE_CATALOG',
              $colColumn = 'COLUMN_NAME',
              $colDataType = 'DATA_TYPE';

    /**
     * Get the constraints and bound values for the select query.
     * 
     * @param array $defaultColumns
     * @return array
     */
    abstract protected function getSelectQueryColumns(array $defaultColumns);

    /**
     * Get the constraints for the information_schema query.
     * 
     * @param string $database
     * @return array
     */
    abstract protected function getSelectQueryConstraints($database);

    /**
     * Execute the select query to information_schema.
     * 
     * @param string $query
     * @param array $values
     * @return array
     */
    abstract protected function executeSelectQuery($query, array $values);

    /**
     * Read the schema from the given PDO instance.
     * 
     * @param string $database
     * @return \Vector5\DB\TreeWalker\Schema
     * 
     * @throws \Vector5\DB\TreeWalker\Exceptions\SchemaReadException
     */
    public function readFromInformationSchema($database)
    {
        // Merge required columns
        $columns = $this->getSelectQueryColumns([
            $this->colCatalog, $this->colTable, $this->colSchema, $this->colColumn, $this->colDataType
        ]);
        
        $columnsSelect = join(',', $columns);
        $query = "SELECT $columnsSelect FROM {$this->table}";
        $values = [];

        if ($constraints = $this->getSelectQueryConstraints($database)) {
            list ($conditions, $values) = $constraints;

            $query .= " WHERE $conditions";
        }

        $query = $this->addSelectQueryOrderBy($query, $columns);

        try {
            $result = $this->executeSelectQuery($query, $values);
        } catch (Exception $e) {
            throw new SchemaReadException($database);
        }

        return $this->createSchema($result, $database);
    }

    /**
     * Get the order by clause of the select query.
     * 
     * @param string $query
     * @param array $columns
     * @return string
     */
    protected function addSelectQueryOrderBy($query, array $columns)
    {
        return $query." ORDER BY {$this->colTable} ASC, {$this->colColumn} ASC";
    }

    /**
     * Create schema from the results of running the query against the information_schema.
     * 
     * @param array $rows
     * @param string $database
     * @return \Vector5\DB\TreeWalker\Schema
     */
    protected function createSchema(array $rows, $database)
    {
        // Translate all keys/column names to lowercase first and group by their table
        $rows = (new Collection($rows))
            ->map(function ($row) {
                return (new Collection($row))->keyBy(function ($row, $key) {
                    return strtoupper($key);
                })->all();
            })
            ->groupBy($this->colTable);
        $schema = new Schema($database);

        foreach ($rows as $tableName => $columns) {
            $table = $schema->addTable($tableName);

            foreach ($columns as $column) {
                $table->addColumn($column[$this->colColumn], $column[$this->colDataType]);
            }
        }

        return $schema;
    }
}