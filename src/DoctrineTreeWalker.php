<?php

namespace Vector5\DB\TreeWalker;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Table as DBALTable;

class DoctrineTreeWalker implements TreeWalker
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $connection;

    /**
     * @param \Doctrine\DBAL\Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get the Doctrine DBAL connection.
     * 
     * @return \Doctrine\DBAL\Connection $connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Load the schema.
     * 
     * @param string $database
     * @param array $config (optional)
     * @return \Vector5\DB\TreeWalker\Schema 
     */
    public function load($database, array $config = [])
    {
        $schema = new Schema($database);
        $tables = $this->getSchemaManager()->listTables();

        foreach ($tables as $doctrineTable) {
            $table = $schema->addTable($doctrineTable->getName());
            $details = $this->getSchemaManager()->listTableDetails($table->getName());
            
            $this->loadColumns($table);
            $this->loadPrimaryKeys($table, $details);
            $this->loadForeignKeys($table, $details);
        }        

        return $schema;
    }

    /**
     * Load all columns of the table.
     * 
     * @param \Vector5\DB\TreeWalker\Table $table
     * @param \Doctrine\DBAL\Schema\Table $details
     * @return \Vector5\DB\TreeWalker\Table
     */
    protected function loadColumns(Table $table)
    {
        $columns =  $this->getSchemaManager()->listTableColumns($table->getName());

        foreach ($columns as $column) {
            $table->addColumn(
                $column->getName(), $column->getType()->getName()
            );
        }
    }

    /**
     * Load primary key of the table.
     *
     * @param \Vector5\DB\TreeWalker\Table $table
     * @param \Doctrine\DBAL\Schema\Table $details
     * @return void
     */
    protected function loadPrimaryKeys(Table $table, DBALTable $details)
    {
        if ($details->hasPrimaryKey()) {
            foreach($details->getPrimaryKey()->getColumns() as $column) {            
                $table->getColumn($column)->setIsPrimaryKey(true);
            }
        }
    }

    /**
     * Load the foreign keys of the table.
     *
     * @param \Vector5\DB\TreeWalker\Table $table
     * @return \Doctrine\DBAL\Schema\Table $details
     * @return void
     */
    protected function loadForeignKeys(Table $table, DBALTable $details)
    {
        foreach ($details->getForeignKeys() as $foreignKey) {
            $localColumn = head($foreignKey->getLocalColumns());
            $foreignColumn = head($foreignKey->getForeignColumns());

            if ($column = $table->getColumn($localColumn)) {
                $column->references($foreignColumn, $foreignKey->getForeignTableName());
            }
        }
    }

    /**
     * Get the Doctrine DBAL schema manager for the connection.
     * 
     * @return \Doctrine\DBAL\Schema\AbstractSchemaManager
     */
    public function getSchemaManager()
    {
        return $this->connection->getSchemaManager();
    }
}