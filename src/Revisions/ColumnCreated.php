<?php

namespace Vector5\DB\TreeWalker\Revisions;

use Vector5\DB\TreeWalker\Column;

class ColumnCreated implements Revision
{
    /**
     * @var \Vector5\DB\TreeWalker\Column
     */
    protected $column;

    /**
     * @param \Vector5\DB\TreeWalker\Column $column
     */
    public function __construct(Column $column)
    {
        $this->column = $column;
    }

    /**
     * Get the dropped column.
     * 
     * @return \Vector5\DB\TreeWalker\Column
     */
    public function getPrevious()
    {
        return null;
    }

    /**
     * Get the new version.
     * 
     * @return mixed
     */
    public function getCurrent()
    {
        return $this->column;
    }

    /**
     * 
     * @return string
     */
    public function __toString()
    {
        return sprintf('Created column %s on %s table', 
            $this->column->getName(), $this->column->getTable()->getQualifiedName()
        );
    }
}