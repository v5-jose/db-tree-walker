<?php

namespace Vector5\DB\TreeWalker\Revisions;

use Vector5\DB\TreeWalker\Table;

class TableCreated implements Revision
{
    /**
     * @var \Vector5\DB\TreeWalker\Table
     */
    protected $table;

    /**
     * @param \Vector5\DB\TreeWalker\Table $table
     */
    public function __construct(Table $table)
    {
        $this->table = $table;
    }

    /**
     * Get the dropped table.
     * 
     * @return \Vector5\DB\TreeWalker\Table
     */
    public function getPrevious()
    {
        return null;
    }

    /**
     * Get the new version.
     * 
     * @return mixed
     */
    public function getCurrent()
    {
        return $this->table;
    }

    /**
     * 
     * @return string
     */
    public function __toString()
    {
        return sprintf('Created table %s on %s', 
            $this->table->getName(), $this->table->getSchema()->getName()
        );
    }
}