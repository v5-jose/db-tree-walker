<?php

namespace Vector5\DB\TreeWalker\Revisions;

use Vector5\DB\TreeWalker\Schema;
use Vector5\DB\TreeWalker\Table;
use Vector5\DB\TreeWalker\Revisions\TableDropped;
use Illuminate\Support\Collection;

class StandardRevisionsIdentifier implements RevisionsIdentifier
{
    /**
     * Get the revisions between two schemas.
     * 
     * @param \Vector5\DB\TreeWalker\Schema $previous
     * @param \Vector5\DB\TreeWalker\Schema $current
     * @return array
     */
    public function revisions(Schema $previous, Schema $current)
    {
        $commonTables = $this->common(
            $previousTables = $previous->getTableCollection(), 
            $currentTables = $current->getTableCollection()
        );
        $revisions = new Collection();
        $droppedTables = $this->getDifference($previousTables, $currentTables);
        $createdTables = $this->getDifference($currentTables, $previousTables);

        if ($droppedTables->count() > 0) {
            $revisions = $revisions->concat($droppedTables->mapInto(TableDropped::class));
        }

        if ($createdTables->count() > 0) {
            $revisions = $revisions->concat($createdTables->mapInto(TableCreated::class));
        }

        // Get revisions to the columns of the common tables
        foreach ($commonTables as $previousTable) {
            $tableRevisions = $this->getTableRevisions(
                $previousTable, $currentTables->get($previousTable->getName())
            );
        
            if ($tableRevisions->isNotEmpty()) {
                $revisions = $revisions->concat($tableRevisions);
            }
        }


        return $revisions->all();
    }

    /**
     * Get the list of common items between the two collections.
     * 
     * @param \Illuminate\Support\Collection $previous
     * @param \Illuminate\Support\Collection $current
     * @return \Illuminate\Support\Collection
     */
    protected function common($previous, $current)
    {
        return $previous->intersectByKeys($current);
    }

    /**
     * Get collection of "dropped" from the first collection.
     * 
     * @param \Illuminate\Support\Collection $previous
     * @param \Illuminate\Support\Collection $current
     * @param string $mapInto
     * @return \Illuminate\Support\Collection
     */
    protected function diff($previous, $current, $mapInto = null)
    {
        if (is_null($mapInto)) {
            return $previous
            ->diffKeys($current)
            ->values();
        }

        return $previous
            ->diffKeys($current)
            ->mapInto($mapInto)
            ->values();
    }

    /**
     * Get the list of dropped columns of the given tables.
     * 
     * @param \Vector5\DB\TreeWalker\Table $previous
     * @param \Vector5\DB\TreeWalker\Table $current
     * @return \Illuminate\Support\Collection
     */
    protected function getTableRevisions(Table $previous, Table $current)
    {
        $commonColumns = $this->common(
            $previousColumns = $previous->getColumnCollection(), 
            $currentColumns = $current->getColumnCollection()
        );

        $revisions = new Collection();

        $droppedColumns = $this->getDifference($previousColumns, $currentColumns);
        $createdColumns = $this->getDifference($currentColumns, $previousColumns);
        if ($droppedColumns->count() > 0) {
            $revisions = $revisions->concat($droppedColumns->mapInto(ColumnDropped::class));
        }

        if ($createdColumns->count() > 0) {
            $revisions = $revisions->concat($createdColumns->mapInto(ColumnCreated::class));
        }

        // Get revisions to the table's columns.
        // As of the moment, we only check for changes to the column's data type.
        foreach ($commonColumns as $previousColumn) {
            $currentColumn = $currentColumns->get($previousColumn->getName());

            if ($previousColumn->getDataType() !== $currentColumn->getDataType() ||
                $previousColumn->getIsPrimaryKey() !== $currentColumn->getIsPrimaryKey() ||
                $previousColumn->isForeignKey() !== $currentColumn->isForeignKey()) {
                $revisions->push(new ColumnModified($previousColumn, $currentColumn));
            }            
        }

        return $revisions;
    }

    /**
     * Another differ
     *
     * @param Collection $first
     * @param Collection $second
     * @return Collection
     */
    public function getDifference($first, $second)
    {
        return $first->diffKeys($second);
    }
}
