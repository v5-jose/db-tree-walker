<?php

namespace Vector5\DB\TreeWalker\Revisions;

use Vector5\DB\TreeWalker\Table;

class TableDropped implements Revision
{
    /**
     * @var \Vector5\DB\TreeWalker\Table
     */
    protected $table;

    /**
     * @param \Vector5\DB\TreeWalker\Table $table
     */
    public function __construct(Table $table)
    {
        $this->table = $table;
    }

    /**
     * Get the dropped table.
     * 
     * @return \Vector5\DB\TreeWalker\Table
     */
    public function getPrevious()
    {
        return $this->table;
    }

    /**
     * Get the new version.
     * 
     * @return mixed
     */
    public function getCurrent()
    {
        return null;
    }

    /**
     * 
     * @return string
     */
    public function __toString()
    {
        return sprintf('Dropped %s from %s', 
            $this->table->getName(), $this->table->getSchema()->getName()
        );
    }
}