<?php

namespace Vector5\DB\TreeWalker\Revisions;

use Vector5\DB\TreeWalker\Column;

class ColumnModified implements Revision
{
    /**
     * @var \Vector5\DB\TreeWalker\Column
     */
    protected $previous;

    /**
     * @var \Vector5\DB\TreeWalker\Column
     */
    protected $current;

    /**
     * @param \Vector5\DB\TreeWalker\Column $previous
     * @param \Vector5\DB\TreeWalker\Column $current
     */
    public function __construct(Column $previous, Column $current)
    {
        $this->previous = $previous;
        $this->current = $current;
    }

    /**
     * Get the dropped column.
     * 
     * @return \Vector5\DB\TreeWalker\Column
     */
    public function getPrevious()
    {
        return $this->previous;
    }

    /**
     * Get the new version.
     * 
     * @return mixed
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * 
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            'Modified column %s to %s of %s table',
            $this->formatColumnMessage($this->previous),
            $this->formatColumnMessage($this->current),
            $this->current->getTable()->getQualifiedName()
        );
    }

    /**
     * Format the message for the column.
     *
     * @param \Vector5\DB\TreeWalker\Column $column
     * @return string
     */
    protected function formatColumnMessage(Column $column)
    {
        if ($column->getIsPrimaryKey()) {
            $index = '(PK)';
        } elseif ($column->isForeignKey()) {
            $index = "(FK -> {$column->getQualifiedReferencedColumn()})";
        } else {
            $index = '';
        }

        return sprintf('%s (%s) %s', $column->getName(), $column->getDataType(), $index);
    }
}