<?php

namespace Vector5\DB\TreeWalker\Revisions;

interface Revision
{
    /**
     * Get the source of the revision.
     * 
     * @return mixed
     */
    //public function getSource();

    /**
     * Get the previous/old version.
     * 
     * @return mixed
     */
    public function getPrevious();

    /**
     * Get the new version.
     * 
     * @return mixed
     */
    public function getCurrent();
}
