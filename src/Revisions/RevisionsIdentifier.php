<?php

namespace Vector5\DB\TreeWalker\Revisions;

use Vector5\DB\TreeWalker\Schema;

interface RevisionsIdentifier
{
    /**
     * Get the revisions between two schemas.
     * 
     * @param \Vector5\DB\TreeWalker\Schema $s1
     * @param \Vector5\DB\TreeWalker\Schema $s2
     * @return array
     */
    public function revisions(Schema $s1, Schema $s2);
}