<?php

namespace Vector5\DB\TreeWalker;

use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Arrayable;

class Table implements Arrayable
{
    /**
     * @var \Vector5\DB\TreeWalker\Schema
     */
    protected $schema;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $columns;

    /**
     * 
     * @param \Vector5\DB\TreeWalker\Schema $schema
     * @param string $name
     * @param array $columns
     */
    public function __construct(Schema $schema, $name, array $columns = [])
    {
        $this->schema = $schema;
        $this->name = $name;
        $this->setColumns($columns);
    }

    /**
     * Get the schema where table belongs to.
     * 
     * @return \Vector5\DB\TreeWalker\Schema
     */
    public function getSchema()
    {
        return $this->schema;
    }

    /**
     * Get the name of the table
     * 
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the columns of the table
     * 
     * @return \Vector5\DB\TreeWalker\Column[]
     */
    public function getColumns()
    {
        return $this->columns->all();
    }

    /**
     * Get the underlying column collection
     * 
     * @return \Illuminate\Support\Collection
     */
    public function getColumnCollection()
    {
        return $this->columns;
    }

    /**
     * Get the column by its name
     * 
     * @param string $name
     * @return \Vector5\DB\TreeWalker\Column
     */
    public function getColumn($name)
    {
        return $this->columns->get($name);
    }

    /**
     * Add a column to the table
     * 
     * @param string|\Vector5\DB\TreeWalker\Column $column
     * @param string $dataType (optional)
     * @return \Vector5\DB\TreeWalker\Column
     */
    public function addColumn($column, $dataType = null)
    {
        // Make sure the column was built by the table's schema
        if ($column instanceof Column) {
            if ($column->getTable() !== $this) {
                throw new InvalidArgumentException('Treewalker: Cannot add column from another table');
            }
        } else {
            $column = $this->buildColumn($column, $dataType);
        }

        $this->columns->put($column->getName(), $column);

        return $column;
    }

    /**
     * Build a new column instance
     * 
     * @param string $name
     * @param string $dataType
     * @return \Vector5\DB\TreeWalker\Column
     */
    public function buildColumn($name, $dataType)
    {
        return new Column($this, $name, $dataType);
    }

    /**
     * Set the columns using an array column details.
     * 
     * @param array $columns
     * @return self
     */
    public function setColumns(array $columns)
    {
        // Discard all columns and replace with the new one
        $this->columns = new Collection();

        foreach ($columns as $column) {
            if ($column instanceof Column) {
               $this->addColumn($column);
            } else {
                $this->addColumn($column['name'], $column['data_type']);
            }
        }

        return $this;
    }

    /**
     * Check if the schema has any column
     * 
     * @return bool
     */
    public function hasColumns()
    {
        return $this->columns->isNotEmpty();
    }

    /**
     * Get the fully qualified table name.
     * 
     * @return string
     */
    public function getQualifiedName()
    {
        return $this->schema->getName().'.'.$this->getName();
    }

    /**
     * Undocumented function
     *
     * @param [type] $column
     * @return void
     */
    public function removeColumn($column)
    {
        if ($column instanceof Column) {
            if ($this === $column->getTable()) {
                $this->columns->forget($column->getName());
            }
        } elseif (is_string($column)) {
            $this->columns->forget($column);
        }
    }

    /**
     * Get the table's array representation.
     * 
     * @return array
     */
    public function toArray()
    {
        return [
            'name' => $this->getName(),
            'columns' => $this->columns->values()->toArray(),
            'column_count' => $this->columns->count(),
            'qualified_name' => $this->getQualifiedName()
        ];
    }

    /**
     * Copy column to list
     *
     * @param Column $column
     * @return Column
     */
    public function copyColumn(Column $column)
    {
        $newColumn = $this->addColumn($column->getName(), $column->getDataType());
        $newColumn->setIsPrimaryKey($column->getIsPrimaryKey());
        return $newColumn;
    }

    /**
     * Get the fully qualified name for the given column.
     *
     * @param string $column
     * @return string
     */
    public function qualifyColumn($column)
    {
        return $this->getName().".$column";
    }
}
