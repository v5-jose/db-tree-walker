<?php

namespace Vector5\DB\TreeWalker;

interface TreeWalker
{
    /**
     * Load the schema.
     * 
     * @param string $database
     * @param array $config (optional)
     * @return \Vector5\DB\TreeWalker\Schema 
     */
    public function load($database, array $config = []);
}